import 'package:balance3d/src/app/extra/util/enums.dart';
import 'package:balance3d/src/app/extra/util/navigation.dart';
import 'package:balance3d/src/app/pages/{{filename.snakeCase()}}/src/model.dart';
import 'package:flutter/cupertino.dart';
import 'package:get_it/get_it.dart';

export 'src/model.dart';

part 'src/controller.dart';
part 'src/event.dart';
part 'src/view.dart';
