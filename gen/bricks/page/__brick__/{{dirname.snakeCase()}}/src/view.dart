part of '../index.dart';

class {{classname.pascalCase()}}PageArgs {
APAlbumPageArgs({
required this.title,
});

final String title;
}

class {{classname.pascalCase()}}PageView extends StatefulWidget {
const {{classname.pascalCase()}}PageView({
required this.args,
super.key,
}) : super();

final {{classname.pascalCase()}}PageArgs args;

@override
State<APAlbumPageView> createState() => _{{classname.pascalCase()}}ViewState();
}

class _{{classname.pascalCase()}}ViewState extends State<{{classname.pascalCase()}}PageView> {
@override
void initState() {
super.initState();
}

@override
void dispose() {
context.read<{{classname.pascalCase()}}BlocController>().close();

super.dispose();
}

@override
Widget build(BuildContext context) {
return BlocProvider(
create: (_) => {{classname.pascalCase()}}BlocController(args: widget.args),
child: BlocBuilder<{{classname.pascalCase()}}PageBlocController,{{classname.pascalCase()}}PageStateModel>(builder: (
context,
state,
) {
final {{classname.pascalCase()}}BlocController controller = context.read<{{classname.pascalCase()}}BlocController>();

return Scaffold(
appBar: DefaultAppBar(
title: '',
),
body: SafeArea(
child: Center(
child: Text('Body'),
)
),
);
}),
);
}
}
