part of '../index.dart';

class {{classname.pascalCase()}}PageBlocController extends Bloc<A{{classname.pascalCase()}}PageEvent, {{classname.pascalCase()}}PageStateModel> {
{{classname.pascalCase()}}PageBlocController({
required this.args,
}) : super(
const {{classname.pascalCase()}}PageStateModel(title: ''),
) {
on<{{classname.pascalCase()}}PageInitEvent>(_onInitEvent);

add({{classname.pascalCase()}}PageInitEvent());
}

final HomePageArgs args;

@override
Future<void> close() async {
super.close();
}

void _onInitEvent(
{{classname.pascalCase()}}PageInitEvent event,
Emitter<{{classname.pascalCase()}}PageStateModel> emit,
) {}
}
