part of '../index.dart';

abstract class A{{classname.pascalCase()}}PageEvent {}

final class {{classname.pascalCase()}}PageInitEvent extends A{{classname.pascalCase()}}PageEvent {}
