part of '../index.dart';

class {{classname.pascalCase()}}Controller
extends ValueNotifier<{{classname.pascalCase()}}Model> {
{{classname.pascalCase()}}Controller({
required {{classname.pascalCase()}}Model model,
}) : super(
model,
);

void updateValue(String v) {
value = value.copyWith(value: v);
}
}
