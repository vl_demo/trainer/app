#!/bin/sh

DIR_NAME="song"
CLASS_NAME="APSongProgressPlayBar"
FILE_PATH="/src/components/src/play_bar/extra/progress/"
BRICK_NAME="ui_component"

cd ../../ &&
ls &&
mason make $BRICK_NAME -o ./lib$FILE_PATH --dirname $DIR_NAME --classname $CLASS_NAME

cd scripts/auto_build &&
sh autobuild.sh
