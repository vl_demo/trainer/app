#!/bin/sh

DIR_NAME="order_detail"
CLASS_NAME="OrderDetail"
FILE_PATH="/src/app/pages/"
BRICK_NAME="page"

cd ../../ &&
ls &&
mason make $BRICK_NAME -o ./lib$FILE_PATH --dirname $DIR_NAME --classname $CLASS_NAME

cd scripts/auto_build &&
sh autobuild.sh
