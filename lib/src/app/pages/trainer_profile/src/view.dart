part of '../index.dart';

class TrainerProfilePageView extends StatefulWidget {
  const TrainerProfilePageView({
    super.key,
  }) : super();

  @override
  State<TrainerProfilePageView> createState() => _TrainerProfilePageViewState();
}

class _TrainerProfilePageViewState extends State<TrainerProfilePageView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final backgroundColors = theme.extension<BackgroundColors>()!;

    return BlocProvider(
      create: (_) => TrainerProfilePageBlocController(),
      child: BlocBuilder<TrainerProfilePageBlocController, TrainerProfilePageStateModel>(
        builder: (
          context,
          state,
        ) {
          final TrainerProfilePageBlocController controller = context.read<TrainerProfilePageBlocController>();

          final Widget body;

          final width = MediaQuery.of(context).size.width;

          final int crossAxisCount = width ~/ 180;

          final ScrollController scrollController = ScrollController();

          if (state.loading) {
            body = const Center(
              child: LoaderIndicator.sm(),
            );
          } else {
            body = NestedScrollView(
              controller: scrollController,
              physics: const BouncingScrollPhysics(
                parent: AlwaysScrollableScrollPhysics(),
              ),
              headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                return [
                  SliverPersistentHeader(
                    delegate: PersonalTrainerAppBar(
                      tabBarLabelList: [
                        AppLocalizations.of(context)!.info,
                        AppLocalizations.of(context)!.photo,
                        AppLocalizations.of(context)!.video,
                      ],
                      label: "Foo Bar",
                      expandedHeight: 280.0,
                      imagePath: Assets.imageAvatar1,
                      imageLoadType: ImageLoadType.asset,
                      onBack: () {
                        GetIt.I.get<INavigationService>().routePop();
                      },
                    ),
                    pinned: true,
                    //floating: true,
                  ),
                ];
              },
              body: TabBarView(
                //physics: const NeverScrollableScrollPhysics(),
                children: [
                  ListView(
                    children: [
                      LabelItemWidget(
                        label: AppLocalizations.of(context)!.name,
                        value: state.name ?? '',
                      ),
                      LabelItemWidget(
                        label: AppLocalizations.of(context)!.email,
                        value: state.email ?? '',
                      ),
                      CalendarItemWidget(
                        label: AppLocalizations.of(context)!.date_birth,
                        value: DatetimeFormat.getFormattedDate(
                          dateTime: state.birthday,
                        ),
                      ),
                      CountryItemWidget(
                        label: AppLocalizations.of(context)!.country,
                        value: state.country ?? '',
                      ),
                      ScheduleItemWidget.controller(
                        sunLabel: AppLocalizations.of(context)!.sun,
                        monLabel: AppLocalizations.of(context)!.mon,
                        tueLabel: AppLocalizations.of(context)!.tue,
                        wedLabel: AppLocalizations.of(context)!.wed,
                        thuLabel: AppLocalizations.of(context)!.thu,
                        friLabel: AppLocalizations.of(context)!.fri,
                        satLabel: AppLocalizations.of(context)!.sat,
                        label: AppLocalizations.of(context)!.schedule,
                        controller: controller.scheduleItemController,
                      ),
                      LabelItemWidget(
                        label: AppLocalizations.of(context)!.experience,
                        value: state.experience ?? '',
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(Spacing.spacing2XS),
                    child: GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: crossAxisCount,
                        crossAxisSpacing: Spacing.spacing2XS,
                        mainAxisSpacing: Spacing.spacing2XS,
                      ),
                      itemCount: 100,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {},
                          child: const ImageItemWidget(
                            imageLoadType: ImageLoadType.asset,
                            imagePath: Assets.imageImage1,
                          ),
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(Spacing.spacing2XS),
                    child: GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: crossAxisCount,
                        crossAxisSpacing: Spacing.spacing2XS,
                        mainAxisSpacing: Spacing.spacing2XS,
                      ),
                      itemCount: 100,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {},
                          child: const VideoItemWidget(
                            imageLoadType: ImageLoadType.asset,
                            imagePath: Assets.imageImage1,
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
          }

          return DefaultTabController(
            length: 3,
            child: Scaffold(
              backgroundColor: backgroundColors.white,
              body: SafeArea(
                child: body,
              ),
            ),
          );
        },
      ),
    );
  }
}
