part of '../index.dart';

abstract class ATrainerProfilePageEvent {}

final class TrainerProfileInitLoadEvent extends ATrainerProfilePageEvent {
  TrainerProfileInitLoadEvent();
}
