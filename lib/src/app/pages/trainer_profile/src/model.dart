import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class TrainerProfilePageStateModel with _$TrainerProfilePageStateModel {
  const factory TrainerProfilePageStateModel({
    String? errorLabel,
    required bool loading,
    String? name,
    String? email,
    String? country,
    String? experience,
    DateTime? birthday,
  }) = _TrainerProfilePageStateModel;
}
