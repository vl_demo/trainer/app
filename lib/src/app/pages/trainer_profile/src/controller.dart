part of '../index.dart';

class TrainerProfilePageBlocController extends Bloc<ATrainerProfilePageEvent, TrainerProfilePageStateModel> {
  TrainerProfilePageBlocController()
      : super(
          const TrainerProfilePageStateModel(
            loading: true,
          ),
        ) {
    on<TrainerProfileInitLoadEvent>(_onInitLoadEvent);

    initLoad();
  }

  final ScheduleItemController scheduleItemController = ScheduleItemController.empty();
  final TextEditingController emailTextEditingController = TextEditingController();
  final TextEditingController passwordTextEditingController = TextEditingController();
  final TextEditingController passwordRepeatTextEditingController = TextEditingController();

  void initLoad() {
    add(
      TrainerProfileInitLoadEvent(),
    );
  }

  void _onInitLoadEvent(
    TrainerProfileInitLoadEvent event,
    Emitter<TrainerProfilePageStateModel> emit,
  ) async {
    emit.call(
      state.copyWith(
        loading: true,
      ),
    );

    const ScheduleItemModel scheduleItemModel = ScheduleItemModel(
      map: {
        WeekdayType.sun: ScheduleSubItemModel(
          startDateTime: TimeOfDay(
            hour: 10,
            minute: 30,
          ),
          endDateTime: TimeOfDay(
            hour: 14,
            minute: 30,
          ),
        ),
        WeekdayType.mon: ScheduleSubItemModel(
          startDateTime: TimeOfDay(
            hour: 10,
            minute: 30,
          ),
          endDateTime: TimeOfDay(
            hour: 14,
            minute: 30,
          ),
        ),
        WeekdayType.wed: ScheduleSubItemModel(
          startDateTime: TimeOfDay(
            hour: 10,
            minute: 30,
          ),
          endDateTime: TimeOfDay(
            hour: 14,
            minute: 30,
          ),
        )
      },
    );

    scheduleItemController.value = scheduleItemController.value.copyWith(
      map: scheduleItemModel.map,
    );

    const experience = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, '
        'sed do eiusmod tempor incididunt ut labore et dolore magna '
        'aliqua. Ut enim ad minim veniam, quis nostrud exercitation '
        'ullamco laboris nisi ut aliquip ex ea commodo consequat. '
        'Duis aute irure dolor in reprehenderit in voluptate velit '
        'esse cillum dolore eu fugiat nulla pariatur. '
        'Excepteur sint occaecat cupidatat non proident, '
        'sunt in culpa qui officia deserunt mollit anim id est laborum.';

    emit.call(
      state.copyWith(
        loading: false,
        name: 'Foo Bar',
        email: 'foobar@mail.com',
        experience: experience,
        country: 'Russia',
        birthday: DateTime.utc(
          1986,
          05,
          13,
        ),
      ),
    );
  }
}
