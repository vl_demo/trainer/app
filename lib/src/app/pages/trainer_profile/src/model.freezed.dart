// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$TrainerProfilePageStateModel {
  String? get errorLabel => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  String? get country => throw _privateConstructorUsedError;
  String? get experience => throw _privateConstructorUsedError;
  DateTime? get birthday => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TrainerProfilePageStateModelCopyWith<TrainerProfilePageStateModel>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TrainerProfilePageStateModelCopyWith<$Res> {
  factory $TrainerProfilePageStateModelCopyWith(
          TrainerProfilePageStateModel value,
          $Res Function(TrainerProfilePageStateModel) then) =
      _$TrainerProfilePageStateModelCopyWithImpl<$Res,
          TrainerProfilePageStateModel>;
  @useResult
  $Res call(
      {String? errorLabel,
      bool loading,
      String? name,
      String? email,
      String? country,
      String? experience,
      DateTime? birthday});
}

/// @nodoc
class _$TrainerProfilePageStateModelCopyWithImpl<$Res,
        $Val extends TrainerProfilePageStateModel>
    implements $TrainerProfilePageStateModelCopyWith<$Res> {
  _$TrainerProfilePageStateModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? errorLabel = freezed,
    Object? loading = null,
    Object? name = freezed,
    Object? email = freezed,
    Object? country = freezed,
    Object? experience = freezed,
    Object? birthday = freezed,
  }) {
    return _then(_value.copyWith(
      errorLabel: freezed == errorLabel
          ? _value.errorLabel
          : errorLabel // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      country: freezed == country
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String?,
      experience: freezed == experience
          ? _value.experience
          : experience // ignore: cast_nullable_to_non_nullable
              as String?,
      birthday: freezed == birthday
          ? _value.birthday
          : birthday // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TrainerProfilePageStateModelImplCopyWith<$Res>
    implements $TrainerProfilePageStateModelCopyWith<$Res> {
  factory _$$TrainerProfilePageStateModelImplCopyWith(
          _$TrainerProfilePageStateModelImpl value,
          $Res Function(_$TrainerProfilePageStateModelImpl) then) =
      __$$TrainerProfilePageStateModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String? errorLabel,
      bool loading,
      String? name,
      String? email,
      String? country,
      String? experience,
      DateTime? birthday});
}

/// @nodoc
class __$$TrainerProfilePageStateModelImplCopyWithImpl<$Res>
    extends _$TrainerProfilePageStateModelCopyWithImpl<$Res,
        _$TrainerProfilePageStateModelImpl>
    implements _$$TrainerProfilePageStateModelImplCopyWith<$Res> {
  __$$TrainerProfilePageStateModelImplCopyWithImpl(
      _$TrainerProfilePageStateModelImpl _value,
      $Res Function(_$TrainerProfilePageStateModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? errorLabel = freezed,
    Object? loading = null,
    Object? name = freezed,
    Object? email = freezed,
    Object? country = freezed,
    Object? experience = freezed,
    Object? birthday = freezed,
  }) {
    return _then(_$TrainerProfilePageStateModelImpl(
      errorLabel: freezed == errorLabel
          ? _value.errorLabel
          : errorLabel // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      country: freezed == country
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String?,
      experience: freezed == experience
          ? _value.experience
          : experience // ignore: cast_nullable_to_non_nullable
              as String?,
      birthday: freezed == birthday
          ? _value.birthday
          : birthday // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ));
  }
}

/// @nodoc

class _$TrainerProfilePageStateModelImpl
    implements _TrainerProfilePageStateModel {
  const _$TrainerProfilePageStateModelImpl(
      {this.errorLabel,
      required this.loading,
      this.name,
      this.email,
      this.country,
      this.experience,
      this.birthday});

  @override
  final String? errorLabel;
  @override
  final bool loading;
  @override
  final String? name;
  @override
  final String? email;
  @override
  final String? country;
  @override
  final String? experience;
  @override
  final DateTime? birthday;

  @override
  String toString() {
    return 'TrainerProfilePageStateModel(errorLabel: $errorLabel, loading: $loading, name: $name, email: $email, country: $country, experience: $experience, birthday: $birthday)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TrainerProfilePageStateModelImpl &&
            (identical(other.errorLabel, errorLabel) ||
                other.errorLabel == errorLabel) &&
            (identical(other.loading, loading) || other.loading == loading) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.country, country) || other.country == country) &&
            (identical(other.experience, experience) ||
                other.experience == experience) &&
            (identical(other.birthday, birthday) ||
                other.birthday == birthday));
  }

  @override
  int get hashCode => Object.hash(runtimeType, errorLabel, loading, name, email,
      country, experience, birthday);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TrainerProfilePageStateModelImplCopyWith<
          _$TrainerProfilePageStateModelImpl>
      get copyWith => __$$TrainerProfilePageStateModelImplCopyWithImpl<
          _$TrainerProfilePageStateModelImpl>(this, _$identity);
}

abstract class _TrainerProfilePageStateModel
    implements TrainerProfilePageStateModel {
  const factory _TrainerProfilePageStateModel(
      {final String? errorLabel,
      required final bool loading,
      final String? name,
      final String? email,
      final String? country,
      final String? experience,
      final DateTime? birthday}) = _$TrainerProfilePageStateModelImpl;

  @override
  String? get errorLabel;
  @override
  bool get loading;
  @override
  String? get name;
  @override
  String? get email;
  @override
  String? get country;
  @override
  String? get experience;
  @override
  DateTime? get birthday;
  @override
  @JsonKey(ignore: true)
  _$$TrainerProfilePageStateModelImplCopyWith<
          _$TrainerProfilePageStateModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}
