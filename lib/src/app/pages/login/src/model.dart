import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class LoginPageStateModel with _$LoginPageStateModel {
  const factory LoginPageStateModel({
    String? email,
    String? password,
    String? errorLabel,
    required bool loading,
  }) = _LoginPageStateModel;
}
