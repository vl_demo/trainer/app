part of '../index.dart';

abstract class ALoginPageEvent {}

final class LoginInitLoadEvent extends ALoginPageEvent {
  LoginInitLoadEvent();
}
