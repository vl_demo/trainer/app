// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$LoginPageStateModel {
  String? get email => throw _privateConstructorUsedError;
  String? get password => throw _privateConstructorUsedError;
  String? get errorLabel => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $LoginPageStateModelCopyWith<LoginPageStateModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginPageStateModelCopyWith<$Res> {
  factory $LoginPageStateModelCopyWith(
          LoginPageStateModel value, $Res Function(LoginPageStateModel) then) =
      _$LoginPageStateModelCopyWithImpl<$Res, LoginPageStateModel>;
  @useResult
  $Res call(
      {String? email, String? password, String? errorLabel, bool loading});
}

/// @nodoc
class _$LoginPageStateModelCopyWithImpl<$Res, $Val extends LoginPageStateModel>
    implements $LoginPageStateModelCopyWith<$Res> {
  _$LoginPageStateModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = freezed,
    Object? password = freezed,
    Object? errorLabel = freezed,
    Object? loading = null,
  }) {
    return _then(_value.copyWith(
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      password: freezed == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      errorLabel: freezed == errorLabel
          ? _value.errorLabel
          : errorLabel // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$LoginPageStateModelImplCopyWith<$Res>
    implements $LoginPageStateModelCopyWith<$Res> {
  factory _$$LoginPageStateModelImplCopyWith(_$LoginPageStateModelImpl value,
          $Res Function(_$LoginPageStateModelImpl) then) =
      __$$LoginPageStateModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String? email, String? password, String? errorLabel, bool loading});
}

/// @nodoc
class __$$LoginPageStateModelImplCopyWithImpl<$Res>
    extends _$LoginPageStateModelCopyWithImpl<$Res, _$LoginPageStateModelImpl>
    implements _$$LoginPageStateModelImplCopyWith<$Res> {
  __$$LoginPageStateModelImplCopyWithImpl(_$LoginPageStateModelImpl _value,
      $Res Function(_$LoginPageStateModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = freezed,
    Object? password = freezed,
    Object? errorLabel = freezed,
    Object? loading = null,
  }) {
    return _then(_$LoginPageStateModelImpl(
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      password: freezed == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      errorLabel: freezed == errorLabel
          ? _value.errorLabel
          : errorLabel // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$LoginPageStateModelImpl implements _LoginPageStateModel {
  const _$LoginPageStateModelImpl(
      {this.email, this.password, this.errorLabel, required this.loading});

  @override
  final String? email;
  @override
  final String? password;
  @override
  final String? errorLabel;
  @override
  final bool loading;

  @override
  String toString() {
    return 'LoginPageStateModel(email: $email, password: $password, errorLabel: $errorLabel, loading: $loading)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoginPageStateModelImpl &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.password, password) ||
                other.password == password) &&
            (identical(other.errorLabel, errorLabel) ||
                other.errorLabel == errorLabel) &&
            (identical(other.loading, loading) || other.loading == loading));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, email, password, errorLabel, loading);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoginPageStateModelImplCopyWith<_$LoginPageStateModelImpl> get copyWith =>
      __$$LoginPageStateModelImplCopyWithImpl<_$LoginPageStateModelImpl>(
          this, _$identity);
}

abstract class _LoginPageStateModel implements LoginPageStateModel {
  const factory _LoginPageStateModel(
      {final String? email,
      final String? password,
      final String? errorLabel,
      required final bool loading}) = _$LoginPageStateModelImpl;

  @override
  String? get email;
  @override
  String? get password;
  @override
  String? get errorLabel;
  @override
  bool get loading;
  @override
  @JsonKey(ignore: true)
  _$$LoginPageStateModelImplCopyWith<_$LoginPageStateModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
