part of '../index.dart';

class LoginPageView extends StatefulWidget {
  const LoginPageView({
    super.key,
  }) : super();

  @override
  State<LoginPageView> createState() => _LoginPageViewState();
}

class _LoginPageViewState extends State<LoginPageView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final backgroundColors = theme.extension<BackgroundColors>()!;

    return BlocProvider(
      create: (_) => LoginPageBlocController(),
      child: BlocBuilder<LoginPageBlocController, LoginPageStateModel>(
        builder: (
          context,
          state,
        ) {
          final LoginPageBlocController controller = context.read<LoginPageBlocController>();

          final Widget body;

          if (state.loading) {
            body = const Center(
              child: LoaderIndicator.sm(),
            );
          } else {
            body = SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(
                    height: Sizing.size7XL,
                  ),
                  InputItemWidget(
                    label: AppLocalizations.of(context)!.email,
                    textEditingController: controller.emailTextEditingController,
                  ),
                  InputItemWidget(
                    label: AppLocalizations.of(context)!.password,
                    isSecure: true,
                    textEditingController: controller.passwordTextEditingController,
                  ),
                  ClearButtonWidget(
                    label: AppLocalizations.of(context)!.register_trainer,
                    onPressed: () {
                      GetIt.I.get<INavigationService>().routeRegister();
                    },
                  ),
                  FillButtonWidget(
                    label: AppLocalizations.of(context)!.log_in,
                    onPressed: () async {
                      GetIt.I.get<INavigationService>().routeTrainersList();
                    },
                  ),
                ],
              ),
            );
          }

          return Scaffold(
            backgroundColor: backgroundColors.white,
            appBar: DefaultAppBar(
              title: AppLocalizations.of(context)!.login,
            ),
            body: SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: Spacing.spacing2XS,
                ),
                child: body,
              ),
            ),
          );
        },
      ),
    );
  }
}
