part of '../index.dart';

class LoginPageBlocController extends Bloc<ALoginPageEvent, LoginPageStateModel> {
  LoginPageBlocController()
      : super(
          const LoginPageStateModel(
            loading: true,
          ),
        ) {
    on<LoginInitLoadEvent>(_onInitLoadEvent);

    initLoad();
  }

  final TextEditingController emailTextEditingController = TextEditingController();
  final TextEditingController passwordTextEditingController = TextEditingController();

  void initLoad() {
    add(
      LoginInitLoadEvent(),
    );
  }

  void _onInitLoadEvent(
    LoginInitLoadEvent event,
    Emitter<LoginPageStateModel> emit,
  ) async {
    emit.call(
      state.copyWith(
        loading: true,
      ),
    );

    emit.call(
      state.copyWith(
        loading: false,
      ),
    );
  }
}
