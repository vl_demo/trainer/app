part of '../index.dart';

class RegisterPageView extends StatefulWidget {
  const RegisterPageView({
    super.key,
  }) : super();

  @override
  State<RegisterPageView> createState() => _RegisterPageViewState();
}

class _RegisterPageViewState extends State<RegisterPageView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final backgroundColors = theme.extension<BackgroundColors>()!;

    return BlocProvider(
      create: (_) => RegisterPageBlocController(),
      child: BlocBuilder<RegisterPageBlocController, RegisterPageStateModel>(
        builder: (
          context,
          state,
        ) {
          final RegisterPageBlocController controller = context.read<RegisterPageBlocController>();

          final Widget body;

          if (state.loading) {
            body = const Center(
              child: LoaderIndicator.sm(),
            );
          } else {
            body = SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(
                    height: Sizing.size5XL,
                  ),
                  InputItemWidget(
                    label: AppLocalizations.of(context)!.name,
                    textEditingController: controller.nameTextEditingController,
                  ),
                  InputItemWidget(
                    label: AppLocalizations.of(context)!.email,
                    textEditingController: controller.emailTextEditingController,
                    errorText: state.errorEmailLabel,
                  ),
                  InputItemWidget(
                    label: AppLocalizations.of(context)!.password,
                    isSecure: true,
                    textEditingController: controller.passwordTextEditingController,
                    errorText: state.errorPassLabel,
                  ),
                  InputItemWidget(
                    label: AppLocalizations.of(context)!.re_password,
                    isSecure: true,
                    textEditingController: controller.passwordRepeatTextEditingController,
                  ),
                  FillButtonWidget(
                    label: AppLocalizations.of(context)!.done,
                    onPressed: () {
                      controller.checkInput();
                    },
                  ),
                ],
              ),
            );
          }

          return Scaffold(
            backgroundColor: backgroundColors.white,
            appBar: RegisterAppBar(
              onBack: () {
                GetIt.I.get<INavigationService>().routePop();
              },
              title: AppLocalizations.of(context)!.registration,
            ),
            body: SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: Spacing.spacing2XS),
                child: body,
              ),
            ),
          );
        },
      ),
    );
  }
}
