// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$RegisterPageStateModel {
  String? get errorEmailLabel => throw _privateConstructorUsedError;
  String? get errorPassLabel => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $RegisterPageStateModelCopyWith<RegisterPageStateModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RegisterPageStateModelCopyWith<$Res> {
  factory $RegisterPageStateModelCopyWith(RegisterPageStateModel value,
          $Res Function(RegisterPageStateModel) then) =
      _$RegisterPageStateModelCopyWithImpl<$Res, RegisterPageStateModel>;
  @useResult
  $Res call({String? errorEmailLabel, String? errorPassLabel, bool loading});
}

/// @nodoc
class _$RegisterPageStateModelCopyWithImpl<$Res,
        $Val extends RegisterPageStateModel>
    implements $RegisterPageStateModelCopyWith<$Res> {
  _$RegisterPageStateModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? errorEmailLabel = freezed,
    Object? errorPassLabel = freezed,
    Object? loading = null,
  }) {
    return _then(_value.copyWith(
      errorEmailLabel: freezed == errorEmailLabel
          ? _value.errorEmailLabel
          : errorEmailLabel // ignore: cast_nullable_to_non_nullable
              as String?,
      errorPassLabel: freezed == errorPassLabel
          ? _value.errorPassLabel
          : errorPassLabel // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RegisterPageStateModelImplCopyWith<$Res>
    implements $RegisterPageStateModelCopyWith<$Res> {
  factory _$$RegisterPageStateModelImplCopyWith(
          _$RegisterPageStateModelImpl value,
          $Res Function(_$RegisterPageStateModelImpl) then) =
      __$$RegisterPageStateModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? errorEmailLabel, String? errorPassLabel, bool loading});
}

/// @nodoc
class __$$RegisterPageStateModelImplCopyWithImpl<$Res>
    extends _$RegisterPageStateModelCopyWithImpl<$Res,
        _$RegisterPageStateModelImpl>
    implements _$$RegisterPageStateModelImplCopyWith<$Res> {
  __$$RegisterPageStateModelImplCopyWithImpl(
      _$RegisterPageStateModelImpl _value,
      $Res Function(_$RegisterPageStateModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? errorEmailLabel = freezed,
    Object? errorPassLabel = freezed,
    Object? loading = null,
  }) {
    return _then(_$RegisterPageStateModelImpl(
      errorEmailLabel: freezed == errorEmailLabel
          ? _value.errorEmailLabel
          : errorEmailLabel // ignore: cast_nullable_to_non_nullable
              as String?,
      errorPassLabel: freezed == errorPassLabel
          ? _value.errorPassLabel
          : errorPassLabel // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$RegisterPageStateModelImpl implements _RegisterPageStateModel {
  const _$RegisterPageStateModelImpl(
      {this.errorEmailLabel, this.errorPassLabel, required this.loading});

  @override
  final String? errorEmailLabel;
  @override
  final String? errorPassLabel;
  @override
  final bool loading;

  @override
  String toString() {
    return 'RegisterPageStateModel(errorEmailLabel: $errorEmailLabel, errorPassLabel: $errorPassLabel, loading: $loading)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RegisterPageStateModelImpl &&
            (identical(other.errorEmailLabel, errorEmailLabel) ||
                other.errorEmailLabel == errorEmailLabel) &&
            (identical(other.errorPassLabel, errorPassLabel) ||
                other.errorPassLabel == errorPassLabel) &&
            (identical(other.loading, loading) || other.loading == loading));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, errorEmailLabel, errorPassLabel, loading);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RegisterPageStateModelImplCopyWith<_$RegisterPageStateModelImpl>
      get copyWith => __$$RegisterPageStateModelImplCopyWithImpl<
          _$RegisterPageStateModelImpl>(this, _$identity);
}

abstract class _RegisterPageStateModel implements RegisterPageStateModel {
  const factory _RegisterPageStateModel(
      {final String? errorEmailLabel,
      final String? errorPassLabel,
      required final bool loading}) = _$RegisterPageStateModelImpl;

  @override
  String? get errorEmailLabel;
  @override
  String? get errorPassLabel;
  @override
  bool get loading;
  @override
  @JsonKey(ignore: true)
  _$$RegisterPageStateModelImplCopyWith<_$RegisterPageStateModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}
