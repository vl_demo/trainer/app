import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class RegisterPageStateModel with _$RegisterPageStateModel {
  const factory RegisterPageStateModel({
    String? errorEmailLabel,
    String? errorPassLabel,
    required bool loading,
  }) = _RegisterPageStateModel;
}
