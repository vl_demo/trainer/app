part of '../index.dart';

class RegisterPageBlocController extends Bloc<ARegisterPageEvent, RegisterPageStateModel> {
  RegisterPageBlocController()
      : super(
          const RegisterPageStateModel(
            loading: true,
          ),
        ) {
    on<RegisterInitLoadEvent>(_onInitLoadEvent);
    on<RegisterCheckValidEvent>(_onCheckValidEvent);
    on<RegisterCleanErrorEvent>(_onCleanErrorEvent);

    emailTextEditingController.addListener(() {
      cleanError();
    });

    passwordTextEditingController.addListener(() {
      cleanError();
    });

    initLoad();
  }

  final TextEditingController nameTextEditingController = TextEditingController();
  final TextEditingController emailTextEditingController = TextEditingController();
  final TextEditingController passwordTextEditingController = TextEditingController();
  final TextEditingController passwordRepeatTextEditingController = TextEditingController();

  void initLoad() {
    add(
      RegisterInitLoadEvent(),
    );
  }

  void checkInput() {
    add(
      RegisterCheckValidEvent(),
    );
  }

  void cleanError() {
    add(
      RegisterCleanErrorEvent(),
    );
  }

  void _onInitLoadEvent(
    RegisterInitLoadEvent event,
    Emitter<RegisterPageStateModel> emit,
  ) async {
    emit.call(
      state.copyWith(
        loading: true,
      ),
    );

    emit.call(
      state.copyWith(
        loading: false,
      ),
    );
  }

  void _onCheckValidEvent(
    RegisterCheckValidEvent event,
    Emitter<RegisterPageStateModel> emit,
  ) async {
    final bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(
      emailTextEditingController.text,
    );

    final bool passValid = RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$').hasMatch(
      emailTextEditingController.text,
    );

    emit.call(
      state.copyWith(
        errorEmailLabel: emailValid ? null : 'Incorrect e-mail',
        errorPassLabel: passValid ? null : 'Incorrect password',
      ),
    );
  }

  void _onCleanErrorEvent(
    RegisterCleanErrorEvent event,
    Emitter<RegisterPageStateModel> emit,
  ) async {
    if (state.errorEmailLabel != null || state.errorPassLabel != null) {
      emit.call(
        state.copyWith(
          errorEmailLabel: null,
          errorPassLabel: null,
        ),
      );
    }
  }
}
