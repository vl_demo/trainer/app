part of '../index.dart';

abstract class ARegisterPageEvent {}

final class RegisterInitLoadEvent extends ARegisterPageEvent {
  RegisterInitLoadEvent();
}

final class RegisterCheckValidEvent extends ARegisterPageEvent {
  RegisterCheckValidEvent();
}

final class RegisterCleanErrorEvent extends ARegisterPageEvent {
  RegisterCleanErrorEvent();
}
