part of '../index.dart';

class TrainersListPageView extends StatefulWidget {
  const TrainersListPageView({
    super.key,
  }) : super();

  @override
  State<TrainersListPageView> createState() => _TrainersListPageViewState();
}

class _TrainersListPageViewState extends State<TrainersListPageView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final backgroundColors = theme.extension<BackgroundColors>()!;

    return BlocProvider(
      create: (_) => TrainersListPageBlocController(),
      child: BlocBuilder<TrainersListPageBlocController, TrainersListPageStateModel>(
        builder: (
          context,
          state,
        ) {
          final TrainersListPageBlocController controller = context.read<TrainersListPageBlocController>();

          final Widget body;

          if (state.loading) {
            body = const Center(
              child: LoaderIndicator.sm(),
            );
          } else {
            body = Padding(
              padding: const EdgeInsets.only(top: Spacing.spacing2XS),
              child: ListView.builder(
                itemCount: state.list.length,
                itemBuilder: (BuildContext context, int index) {
                  final item = state.list[index];

                  return Padding(
                    padding: const EdgeInsets.only(bottom: Spacing.spacing2XS),
                    child: TrainerItemWidget(
                      imagePath: item.imagePath,
                      name: item.name,
                      imageLoadType: ImageLoadType.asset,
                      onTap: () {
                        GetIt.I.get<INavigationService>().routeTrainerProfile();
                      },
                    ),
                  );
                },
              ),
            );
          }

          return Scaffold(
            backgroundColor: backgroundColors.white,
            appBar: TrainersListAppBar(
              title: AppLocalizations.of(context)!.trainers,
              onPersonTap: () {
                GetIt.I.get<INavigationService>().routePersonalProfile();
              },
            ),
            body: SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: Spacing.spacing2XS,
                ),
                child: body,
              ),
            ),
          );
        },
      ),
    );
  }
}
