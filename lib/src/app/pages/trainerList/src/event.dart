part of '../index.dart';

abstract class ATrainersListPageEvent {}

final class TrainersListInitLoadEvent extends ATrainersListPageEvent {
  TrainersListInitLoadEvent();
}
