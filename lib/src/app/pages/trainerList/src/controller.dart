part of '../index.dart';

class TrainersListPageBlocController extends Bloc<ATrainersListPageEvent, TrainersListPageStateModel> {
  TrainersListPageBlocController()
      : super(
          const TrainersListPageStateModel(
            list: [],
            loading: true,
          ),
        ) {
    on<TrainersListInitLoadEvent>(_onInitLoadEvent);

    initLoad();
  }

  void initLoad() {
    add(
      TrainersListInitLoadEvent(),
    );
  }

  void _onInitLoadEvent(
    TrainersListInitLoadEvent event,
    Emitter<TrainersListPageStateModel> emit,
  ) async {
    emit.call(
      state.copyWith(
        loading: true,
      ),
    );

    final List<TrainerItemModel> list = List<TrainerItemModel>.generate(
      100,
      (index) => const TrainerItemModel(
        imagePath: Assets.imageAvatar1,
        name: 'Foobar',
      ),
    );

    emit.call(
      state.copyWith(
        loading: false,
        list: list,
      ),
    );
  }
}
