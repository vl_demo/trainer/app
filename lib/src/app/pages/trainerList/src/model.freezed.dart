// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$TrainerItemModel {
  String get imagePath => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TrainerItemModelCopyWith<TrainerItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TrainerItemModelCopyWith<$Res> {
  factory $TrainerItemModelCopyWith(
          TrainerItemModel value, $Res Function(TrainerItemModel) then) =
      _$TrainerItemModelCopyWithImpl<$Res, TrainerItemModel>;
  @useResult
  $Res call({String imagePath, String name});
}

/// @nodoc
class _$TrainerItemModelCopyWithImpl<$Res, $Val extends TrainerItemModel>
    implements $TrainerItemModelCopyWith<$Res> {
  _$TrainerItemModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? imagePath = null,
    Object? name = null,
  }) {
    return _then(_value.copyWith(
      imagePath: null == imagePath
          ? _value.imagePath
          : imagePath // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TrainerItemModelImplCopyWith<$Res>
    implements $TrainerItemModelCopyWith<$Res> {
  factory _$$TrainerItemModelImplCopyWith(_$TrainerItemModelImpl value,
          $Res Function(_$TrainerItemModelImpl) then) =
      __$$TrainerItemModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String imagePath, String name});
}

/// @nodoc
class __$$TrainerItemModelImplCopyWithImpl<$Res>
    extends _$TrainerItemModelCopyWithImpl<$Res, _$TrainerItemModelImpl>
    implements _$$TrainerItemModelImplCopyWith<$Res> {
  __$$TrainerItemModelImplCopyWithImpl(_$TrainerItemModelImpl _value,
      $Res Function(_$TrainerItemModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? imagePath = null,
    Object? name = null,
  }) {
    return _then(_$TrainerItemModelImpl(
      imagePath: null == imagePath
          ? _value.imagePath
          : imagePath // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$TrainerItemModelImpl implements _TrainerItemModel {
  const _$TrainerItemModelImpl({required this.imagePath, required this.name});

  @override
  final String imagePath;
  @override
  final String name;

  @override
  String toString() {
    return 'TrainerItemModel(imagePath: $imagePath, name: $name)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TrainerItemModelImpl &&
            (identical(other.imagePath, imagePath) ||
                other.imagePath == imagePath) &&
            (identical(other.name, name) || other.name == name));
  }

  @override
  int get hashCode => Object.hash(runtimeType, imagePath, name);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TrainerItemModelImplCopyWith<_$TrainerItemModelImpl> get copyWith =>
      __$$TrainerItemModelImplCopyWithImpl<_$TrainerItemModelImpl>(
          this, _$identity);
}

abstract class _TrainerItemModel implements TrainerItemModel {
  const factory _TrainerItemModel(
      {required final String imagePath,
      required final String name}) = _$TrainerItemModelImpl;

  @override
  String get imagePath;
  @override
  String get name;
  @override
  @JsonKey(ignore: true)
  _$$TrainerItemModelImplCopyWith<_$TrainerItemModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$TrainersListPageStateModel {
  List<TrainerItemModel> get list => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TrainersListPageStateModelCopyWith<TrainersListPageStateModel>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TrainersListPageStateModelCopyWith<$Res> {
  factory $TrainersListPageStateModelCopyWith(TrainersListPageStateModel value,
          $Res Function(TrainersListPageStateModel) then) =
      _$TrainersListPageStateModelCopyWithImpl<$Res,
          TrainersListPageStateModel>;
  @useResult
  $Res call({List<TrainerItemModel> list, bool loading});
}

/// @nodoc
class _$TrainersListPageStateModelCopyWithImpl<$Res,
        $Val extends TrainersListPageStateModel>
    implements $TrainersListPageStateModelCopyWith<$Res> {
  _$TrainersListPageStateModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? list = null,
    Object? loading = null,
  }) {
    return _then(_value.copyWith(
      list: null == list
          ? _value.list
          : list // ignore: cast_nullable_to_non_nullable
              as List<TrainerItemModel>,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TrainersListPageStateModelImplCopyWith<$Res>
    implements $TrainersListPageStateModelCopyWith<$Res> {
  factory _$$TrainersListPageStateModelImplCopyWith(
          _$TrainersListPageStateModelImpl value,
          $Res Function(_$TrainersListPageStateModelImpl) then) =
      __$$TrainersListPageStateModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<TrainerItemModel> list, bool loading});
}

/// @nodoc
class __$$TrainersListPageStateModelImplCopyWithImpl<$Res>
    extends _$TrainersListPageStateModelCopyWithImpl<$Res,
        _$TrainersListPageStateModelImpl>
    implements _$$TrainersListPageStateModelImplCopyWith<$Res> {
  __$$TrainersListPageStateModelImplCopyWithImpl(
      _$TrainersListPageStateModelImpl _value,
      $Res Function(_$TrainersListPageStateModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? list = null,
    Object? loading = null,
  }) {
    return _then(_$TrainersListPageStateModelImpl(
      list: null == list
          ? _value._list
          : list // ignore: cast_nullable_to_non_nullable
              as List<TrainerItemModel>,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$TrainersListPageStateModelImpl implements _TrainersListPageStateModel {
  const _$TrainersListPageStateModelImpl(
      {required final List<TrainerItemModel> list, required this.loading})
      : _list = list;

  final List<TrainerItemModel> _list;
  @override
  List<TrainerItemModel> get list {
    if (_list is EqualUnmodifiableListView) return _list;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_list);
  }

  @override
  final bool loading;

  @override
  String toString() {
    return 'TrainersListPageStateModel(list: $list, loading: $loading)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TrainersListPageStateModelImpl &&
            const DeepCollectionEquality().equals(other._list, _list) &&
            (identical(other.loading, loading) || other.loading == loading));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_list), loading);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TrainersListPageStateModelImplCopyWith<_$TrainersListPageStateModelImpl>
      get copyWith => __$$TrainersListPageStateModelImplCopyWithImpl<
          _$TrainersListPageStateModelImpl>(this, _$identity);
}

abstract class _TrainersListPageStateModel
    implements TrainersListPageStateModel {
  const factory _TrainersListPageStateModel(
      {required final List<TrainerItemModel> list,
      required final bool loading}) = _$TrainersListPageStateModelImpl;

  @override
  List<TrainerItemModel> get list;
  @override
  bool get loading;
  @override
  @JsonKey(ignore: true)
  _$$TrainersListPageStateModelImplCopyWith<_$TrainersListPageStateModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}
