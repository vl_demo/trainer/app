import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class TrainerItemModel with _$TrainerItemModel {
  const factory TrainerItemModel({
    required String imagePath,
    required String name,
  }) = _TrainerItemModel;
}

@freezed
class TrainersListPageStateModel with _$TrainersListPageStateModel {
  const factory TrainersListPageStateModel({
    required List<TrainerItemModel> list,
    required bool loading,
  }) = _TrainersListPageStateModel;
}
