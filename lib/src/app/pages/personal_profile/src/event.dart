part of '../index.dart';

abstract class APersonalProfilePageEvent {}

final class PersonalProfileInitLoadEvent extends APersonalProfilePageEvent {
  PersonalProfileInitLoadEvent();
}

final class PersonalProfileSetBirthdayEvent extends APersonalProfilePageEvent {
  PersonalProfileSetBirthdayEvent({
    required this.birthday,
  });

  final DateTime birthday;
}

final class PersonalProfileSetTimeScheduleEvent extends APersonalProfilePageEvent {
  PersonalProfileSetTimeScheduleEvent({
    required this.timeOfDay,
    required this.timeScheduleType,
    required this.weekdayType,
  });

  final TimeOfDay timeOfDay;
  final TimeScheduleType timeScheduleType;
  final WeekdayType weekdayType;
}

final class PersonalProfileSetCountryEvent extends APersonalProfilePageEvent {
  PersonalProfileSetCountryEvent({
    required this.country,
  });

  final Country country;
}
