import 'package:country_picker/country_picker.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class PersonalProfilePageStateModel with _$PersonalProfilePageStateModel {
  const factory PersonalProfilePageStateModel({
    String? errorLabel,
    required bool loading,
    Country? country,
    DateTime? birthday,
  }) = _PersonalProfilePageStateModel;
}
