part of '../index.dart';

class PersonalProfilePageView extends StatefulWidget {
  const PersonalProfilePageView({
    super.key,
  }) : super();

  @override
  State<PersonalProfilePageView> createState() => _PersonalProfilePageViewState();
}

class _PersonalProfilePageViewState extends State<PersonalProfilePageView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final backgroundColors = theme.extension<BackgroundColors>()!;

    return BlocProvider(
      create: (_) => PersonalProfilePageBlocController(),
      child: BlocBuilder<PersonalProfilePageBlocController, PersonalProfilePageStateModel>(
        builder: (
          context,
          state,
        ) {
          final PersonalProfilePageBlocController controller = context.read<PersonalProfilePageBlocController>();

          final Widget body;

          final width = MediaQuery.of(context).size.width;

          final int crossAxisCount = width ~/ 180;

          final ScrollController scrollController = ScrollController();

          if (state.loading) {
            body = const Center(
              child: LoaderIndicator.sm(),
            );
          } else {
            body = NestedScrollView(
              controller: scrollController,
              physics: const BouncingScrollPhysics(
                parent: AlwaysScrollableScrollPhysics(),
              ),
              headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                return [
                  SliverPersistentHeader(
                    delegate: PersonalTrainerAppBar(
                      isPersonal: true,
                      tabBarLabelList: [
                        AppLocalizations.of(context)!.info,
                        AppLocalizations.of(context)!.photo,
                        AppLocalizations.of(context)!.video,
                      ],
                      label: "Foo Bar",
                      expandedHeight: 280.0,
                      imagePath: Assets.imageAvatar1,
                      imageLoadType: ImageLoadType.asset,
                      onBack: () {
                        GetIt.I.get<INavigationService>().routePop();
                      },
                    ),
                    pinned: true,
                    //floating: true,
                  ),
                ];
              },
              body: TabBarView(
                //physics: const NeverScrollableScrollPhysics(),
                children: [
                  ListView(
                    children: [
                      InputItemWidget(
                        label: AppLocalizations.of(context)!.name,
                        textEditingController: controller.nameTextEditingController,
                      ),
                      InputItemWidget(
                        label: AppLocalizations.of(context)!.email,
                        textEditingController: controller.emailTextEditingController,
                      ),
                      CalendarItemWidget(
                        label: AppLocalizations.of(context)!.date_birth,
                        value: DatetimeFormat.getFormattedDate(
                          dateTime: state.birthday,
                        ),
                        onTap: () async {
                          final date = await showDatePicker(
                            context: context,
                            firstDate: DateTime(1900),
                            lastDate: DateTime.now(),
                          );

                          controller.setBirthday(date ?? DateTime.now());
                        },
                      ),
                      CountryItemWidget(
                        label: AppLocalizations.of(context)!.country,
                        value: controller.state.country?.name ?? AppLocalizations.of(context)!.none,
                        onTap: () {
                          showCountryPicker(
                            context: context,
                            onSelect: (Country country) {
                              controller.setCountry(country);
                            },
                          );
                        },
                      ),
                      ScheduleItemWidget.controller(
                        sunLabel: AppLocalizations.of(context)!.sun,
                        monLabel: AppLocalizations.of(context)!.mon,
                        tueLabel: AppLocalizations.of(context)!.tue,
                        wedLabel: AppLocalizations.of(context)!.wed,
                        thuLabel: AppLocalizations.of(context)!.thu,
                        friLabel: AppLocalizations.of(context)!.fri,
                        satLabel: AppLocalizations.of(context)!.sat,
                        label: AppLocalizations.of(context)!.schedule,
                        isEdit: true,
                        controller: controller.scheduleItemController,
                        onScheduleTap: (
                          TimeScheduleType timeScheduleType,
                          WeekdayType weekdayType,
                        ) async {
                          final timeOfDay = await showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return TimePickerDialog(
                                initialTime: TimeOfDay.fromDateTime(
                                  DateTime.now(),
                                ),
                              );
                            },
                          );

                          controller.setTime(
                            timeOfDay,
                            timeScheduleType,
                            weekdayType,
                          );
                        },
                      ),
                      InputMultilineItemWidget.controller(
                        label: AppLocalizations.of(context)!.experience,
                        textEditingController: controller.experienceTextEditingController,
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(Spacing.spacing2XS),
                    child: GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: crossAxisCount,
                        crossAxisSpacing: Spacing.spacing2XS,
                        mainAxisSpacing: Spacing.spacing2XS,
                      ),
                      itemCount: 100,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {},
                          child: const ImageItemWidget(
                            imageLoadType: ImageLoadType.asset,
                            imagePath: Assets.imageImage1,
                          ),
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(Spacing.spacing2XS),
                    child: GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: crossAxisCount,
                        crossAxisSpacing: Spacing.spacing2XS,
                        mainAxisSpacing: Spacing.spacing2XS,
                      ),
                      itemCount: 100,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {},
                          child: const VideoItemWidget(
                            imageLoadType: ImageLoadType.asset,
                            imagePath: Assets.imageImage1,
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
          }

          return DefaultTabController(
            length: 3,
            child: Scaffold(
              backgroundColor: backgroundColors.white,
              body: SafeArea(
                child: body,
              ),
            ),
          );
        },
      ),
    );
  }
}
