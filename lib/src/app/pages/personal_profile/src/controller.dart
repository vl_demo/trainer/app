part of '../index.dart';

class PersonalProfilePageBlocController extends Bloc<APersonalProfilePageEvent, PersonalProfilePageStateModel> {
  PersonalProfilePageBlocController()
      : super(
          const PersonalProfilePageStateModel(
            loading: true,
          ),
        ) {
    on<PersonalProfileInitLoadEvent>(_onInitLoadEvent);
    on<PersonalProfileSetTimeScheduleEvent>(_onSetTimeEvent);
    on<PersonalProfileSetBirthdayEvent>(_onSetBirthdayEvent);
    on<PersonalProfileSetCountryEvent>(_onSetCountryEvent);

    initLoad();
  }

  final ScheduleItemController scheduleItemController = ScheduleItemController.empty();

  final TextEditingController emailTextEditingController = TextEditingController();
  final TextEditingController nameTextEditingController = TextEditingController();
  final TextEditingController experienceTextEditingController = TextEditingController();

  void initLoad() {
    add(
      PersonalProfileInitLoadEvent(),
    );
  }

  void setTime(
    TimeOfDay timeOfDay,
    TimeScheduleType timeScheduleType,
    WeekdayType weekdayType,
  ) {
    add(
      PersonalProfileSetTimeScheduleEvent(
        timeOfDay: timeOfDay,
        timeScheduleType: timeScheduleType,
        weekdayType: weekdayType,
      ),
    );
  }

  void setBirthday(
    DateTime birthday,
  ) {
    add(
      PersonalProfileSetBirthdayEvent(
        birthday: birthday,
      ),
    );
  }

  void setCountry(
    Country country,
  ) {
    add(
      PersonalProfileSetCountryEvent(
        country: country,
      ),
    );
  }

  void _onInitLoadEvent(
    PersonalProfileInitLoadEvent event,
    Emitter<PersonalProfilePageStateModel> emit,
  ) async {
    emit.call(
      state.copyWith(
        loading: true,
      ),
    );

    const experience = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, '
        'sed do eiusmod tempor incididunt ut labore et dolore magna '
        'aliqua. Ut enim ad minim veniam, quis nostrud exercitation '
        'ullamco laboris nisi ut aliquip ex ea commodo consequat. '
        'Duis aute irure dolor in reprehenderit in voluptate velit '
        'esse cillum dolore eu fugiat nulla pariatur. '
        'Excepteur sint occaecat cupidatat non proident, '
        'sunt in culpa qui officia deserunt mollit anim id est laborum.';

    emailTextEditingController.text = 'foobar@mail.com';
    nameTextEditingController.text = 'Foo Bar';
    experienceTextEditingController.text = experience;

    const ScheduleItemModel scheduleItemModel = ScheduleItemModel(
      map: {
        WeekdayType.sun: ScheduleSubItemModel(
          startDateTime: TimeOfDay(
            hour: 10,
            minute: 30,
          ),
          endDateTime: TimeOfDay(
            hour: 14,
            minute: 30,
          ),
        ),
        WeekdayType.mon: ScheduleSubItemModel(
          startDateTime: TimeOfDay(
            hour: 11,
            minute: 30,
          ),
          endDateTime: TimeOfDay(
            hour: 14,
            minute: 42,
          ),
        ),
        WeekdayType.wed: ScheduleSubItemModel(
          startDateTime: TimeOfDay(
            hour: 13,
            minute: 43,
          ),
          endDateTime: TimeOfDay(
            hour: 17,
            minute: 30,
          ),
        )
      },
    );

    scheduleItemController.value = scheduleItemController.value.copyWith(
      map: scheduleItemModel.map,
    );

    emit.call(
      state.copyWith(
        loading: false,
        birthday: DateTime(1986, 03, 16),
        country: Country.parse('ru'),
      ),
    );
  }

  void _onSetTimeEvent(
    PersonalProfileSetTimeScheduleEvent event,
    Emitter<PersonalProfilePageStateModel> emit,
  ) async {
    scheduleItemController.setTime(
      event.timeScheduleType,
      event.weekdayType,
      event.timeOfDay,
    );
  }

  void _onSetBirthdayEvent(
    PersonalProfileSetBirthdayEvent event,
    Emitter<PersonalProfilePageStateModel> emit,
  ) async {
    emit.call(state.copyWith(
      birthday: event.birthday,
    ));
  }

  void _onSetCountryEvent(
    PersonalProfileSetCountryEvent event,
    Emitter<PersonalProfilePageStateModel> emit,
  ) async {
    emit.call(
      state.copyWith(
        country: event.country,
      ),
    );
  }
}
