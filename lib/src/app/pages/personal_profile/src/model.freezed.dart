// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$PersonalProfilePageStateModel {
  String? get errorLabel => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;
  Country? get country => throw _privateConstructorUsedError;
  DateTime? get birthday => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PersonalProfilePageStateModelCopyWith<PersonalProfilePageStateModel>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PersonalProfilePageStateModelCopyWith<$Res> {
  factory $PersonalProfilePageStateModelCopyWith(
          PersonalProfilePageStateModel value,
          $Res Function(PersonalProfilePageStateModel) then) =
      _$PersonalProfilePageStateModelCopyWithImpl<$Res,
          PersonalProfilePageStateModel>;
  @useResult
  $Res call(
      {String? errorLabel, bool loading, Country? country, DateTime? birthday});
}

/// @nodoc
class _$PersonalProfilePageStateModelCopyWithImpl<$Res,
        $Val extends PersonalProfilePageStateModel>
    implements $PersonalProfilePageStateModelCopyWith<$Res> {
  _$PersonalProfilePageStateModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? errorLabel = freezed,
    Object? loading = null,
    Object? country = freezed,
    Object? birthday = freezed,
  }) {
    return _then(_value.copyWith(
      errorLabel: freezed == errorLabel
          ? _value.errorLabel
          : errorLabel // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      country: freezed == country
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as Country?,
      birthday: freezed == birthday
          ? _value.birthday
          : birthday // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$PersonalProfilePageStateModelImplCopyWith<$Res>
    implements $PersonalProfilePageStateModelCopyWith<$Res> {
  factory _$$PersonalProfilePageStateModelImplCopyWith(
          _$PersonalProfilePageStateModelImpl value,
          $Res Function(_$PersonalProfilePageStateModelImpl) then) =
      __$$PersonalProfilePageStateModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String? errorLabel, bool loading, Country? country, DateTime? birthday});
}

/// @nodoc
class __$$PersonalProfilePageStateModelImplCopyWithImpl<$Res>
    extends _$PersonalProfilePageStateModelCopyWithImpl<$Res,
        _$PersonalProfilePageStateModelImpl>
    implements _$$PersonalProfilePageStateModelImplCopyWith<$Res> {
  __$$PersonalProfilePageStateModelImplCopyWithImpl(
      _$PersonalProfilePageStateModelImpl _value,
      $Res Function(_$PersonalProfilePageStateModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? errorLabel = freezed,
    Object? loading = null,
    Object? country = freezed,
    Object? birthday = freezed,
  }) {
    return _then(_$PersonalProfilePageStateModelImpl(
      errorLabel: freezed == errorLabel
          ? _value.errorLabel
          : errorLabel // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      country: freezed == country
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as Country?,
      birthday: freezed == birthday
          ? _value.birthday
          : birthday // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ));
  }
}

/// @nodoc

class _$PersonalProfilePageStateModelImpl
    implements _PersonalProfilePageStateModel {
  const _$PersonalProfilePageStateModelImpl(
      {this.errorLabel, required this.loading, this.country, this.birthday});

  @override
  final String? errorLabel;
  @override
  final bool loading;
  @override
  final Country? country;
  @override
  final DateTime? birthday;

  @override
  String toString() {
    return 'PersonalProfilePageStateModel(errorLabel: $errorLabel, loading: $loading, country: $country, birthday: $birthday)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PersonalProfilePageStateModelImpl &&
            (identical(other.errorLabel, errorLabel) ||
                other.errorLabel == errorLabel) &&
            (identical(other.loading, loading) || other.loading == loading) &&
            (identical(other.country, country) || other.country == country) &&
            (identical(other.birthday, birthday) ||
                other.birthday == birthday));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, errorLabel, loading, country, birthday);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PersonalProfilePageStateModelImplCopyWith<
          _$PersonalProfilePageStateModelImpl>
      get copyWith => __$$PersonalProfilePageStateModelImplCopyWithImpl<
          _$PersonalProfilePageStateModelImpl>(this, _$identity);
}

abstract class _PersonalProfilePageStateModel
    implements PersonalProfilePageStateModel {
  const factory _PersonalProfilePageStateModel(
      {final String? errorLabel,
      required final bool loading,
      final Country? country,
      final DateTime? birthday}) = _$PersonalProfilePageStateModelImpl;

  @override
  String? get errorLabel;
  @override
  bool get loading;
  @override
  Country? get country;
  @override
  DateTime? get birthday;
  @override
  @JsonKey(ignore: true)
  _$$PersonalProfilePageStateModelImplCopyWith<
          _$PersonalProfilePageStateModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}
