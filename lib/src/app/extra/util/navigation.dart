import 'package:app/src/app/pages/login/index.dart';
import 'package:app/src/app/pages/personal_profile/index.dart';
import 'package:app/src/app/pages/register/index.dart';
import 'package:app/src/app/pages/trainerList/index.dart';
import 'package:app/src/app/pages/trainer_profile/index.dart';
import 'package:flutter/material.dart';

class PathRoutes {
  static const login = '/login';
  static const register = '/register';
  static const trainersList = '/trainers_list';
  static const personalProfile = '/personal_profile';
  static const trainerProfile = '/trainer_profile';
}

abstract class INavigationService {
  final Map<String, Widget Function(BuildContext)> routes = {};

  RouteFactory? get onGenerateRoute;

  String get initRouteName;

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  /// Routes
  bool canPop();
  void routePop();
  void routeLogin();
  void routeRegister();
  void routeTrainersList();
  void routePersonalProfile();
  void routeTrainerProfile();
}

class NavigationService implements INavigationService {
  @override
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  String get initRouteName => PathRoutes.login;

  @override
  final routes = <String, Widget Function(BuildContext)>{
    PathRoutes.login: (context) => const LoginPageView(),
    PathRoutes.register: (context) => const RegisterPageView(),
    PathRoutes.trainersList: (context) => const TrainersListPageView(),
    PathRoutes.personalProfile: (context) => const PersonalProfilePageView(),
    PathRoutes.trainerProfile: (context) => const TrainerProfilePageView(),
  };

  @override
  RouteFactory? get onGenerateRoute => (settings) {
        return null;
      };

  @override
  void routePop() => Navigator.pop(navigatorKey.currentContext!);

  @override
  void routeLogin() {
    Navigator.pushReplacementNamed(
      navigatorKey.currentContext!,
      PathRoutes.login,
    );
  }

  @override
  bool canPop() {
    return Navigator.canPop(navigatorKey.currentContext!);
  }

  @override
  void routeRegister() => Navigator.pushNamed(
        navigatorKey.currentContext!,
        PathRoutes.register,
      );

  @override
  void routeTrainersList() => Navigator.pushNamed(
        navigatorKey.currentContext!,
        PathRoutes.trainersList,
      );

  @override
  void routePersonalProfile() => Navigator.pushNamed(
        navigatorKey.currentContext!,
        PathRoutes.personalProfile,
      );

  @override
  void routeTrainerProfile() => Navigator.pushNamed(
        navigatorKey.currentContext!,
        PathRoutes.trainerProfile,
      );
}
